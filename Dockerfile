FROM redhat/ubi8:8.10 as build

RUN curl https://sh.rustup.rs -ssf | \
    sh -s -- --default-toolchain 1.82 -y &&\
    export PATH=/root/.cargo/bin:$PATH

WORKDIR /build/
COPY . /build/
RUN PATH=$PATH:/root/.cargo/bin cargo build --release

FROM redhat/ubi8-minimal:8.10
WORKDIR /app/

LABEL tethys.processor.name="json-argonauts"
LABEL tethys.processor.summary="It looks at JSON message payload, parses it and decorates it with message attributes given in `expand` paths."
LABEL tethys.processor.type="processor"
LABEL tethys.processor.stream=true

LABEL tethys.processor.property.expansions=false
LABEL tethys.processor.property.expansions.type=string
LABEL tethys.processor.property.expansions.required=true
LABEL tethys.processor.property.expansions.env=EXPANSIONS
LABEL tethys.processor.dynamic_updates=false

COPY --from=build /build/target/release/json-argonauts /app/

CMD ["/app/mpegts-framer"]

