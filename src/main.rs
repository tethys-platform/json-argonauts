use log::{error, debug};
use std::collections::HashMap;

use tethys_common::*;

// JsonArgonauts a Tethys processor 
// It looks at JSON message payload, parses it and decorates it with message attributes
// given in `expand` paths.
// I.e. message: `{id: "hello"}` will be decorated with Tethys attribute ("id": "hello")
// if we configure the service with `expand: ["id"]`;
#[derive(Clone)]
struct JsonArgonauts {
    expand: Vec<String>
}

impl TethysProcessor for JsonArgonauts {
    fn process_request(&self, req: ProcessDataRequest) -> TethysProcessorResult {
        let content = req.content;
        let attributes = get_attributes(&content, &self.expand)
            .map_err(|e| { 
                error!("error parsing content: {e}"); 
                e 
            }).unwrap_or_else(|_| Default::default());

        TethysProcessorResult::processed(req.id, vec![(content, attributes)])
    }
}

fn find_in_json<'a, Iter>(json: &'a serde_json::Value, tree: &mut Iter) -> Option<serde_json::Value>
where
    Iter: std::iter::Iterator<Item=&'a str>
{
    use serde_json::value::Value;

    match tree.next() {
        None => Some(json.clone()),
        Some(name) if name.starts_with("#") => {
            let name = name.strip_prefix("#").unwrap().to_string();
            match json {
                Value::Array(arr) if !name.is_empty() => {
                    let v = arr.iter()
                        .filter_map(|v| find_in_json(v, &mut name.split(".")))
                        .collect::<Vec<_>>();
                    if v.is_empty() { None } else { Some(Value::Array(v)) }
                },
                Value::Array(arr) if name.is_empty() => { // when the name is empty '#', append all keys to the summary
                    let v = arr.iter()
                        .filter_map(|v| {
                            match v {
                                Value::Object(obj) => {
                                   Some(obj.iter().map(|(k, _v)| Value::String(k.clone())))
                                },
                                _ => {
                                    None
                                }
                            }
                        }).flatten()
                        .collect::<Vec<_>>();

                    if v.is_empty() { None } else { Some(Value::Array(v)) }
                },
                _ => None,
            }
        },
        Some(name) => {
            match json {
                Value::Object(map) => {
                    if let Some(res) = map.get(name) {
                        find_in_json(res, tree)
                    } else {
                        None
                    }
                },
                _ => None,
            }
        }
    }
}

#[test]
fn test_find_in_json() {
    let json = r#"{ "name": "Test string", "properties": { "viscosity": 76, "speed": 128, "array": [{ "type": "one" }, { "type": "two" }, { "type": "three" }] }}"#;

    let obj: serde_json::Value = serde_json::from_str(json).expect("failed to parse");
    let res = find_in_json(&obj, &mut "name".split("."));
    assert!(res.is_some());
    let res = find_in_json(&obj, &mut "properties.viscosity".split("."));
    assert!(res.is_some());
    let res = find_in_json(&obj, &mut "properties.viscosity.test2".split("."));
    assert!(res.is_none());

    let res = find_in_json(&obj, &mut "properties.array.#type".split("."));
    assert!(res.is_some());

    let gmti_json = r#"{"packet":{"version_id":"31","nationality":"US","classification":"UNCLASSIFIED","class_system":"US","class_code":["LIMDIS","REL NATO"],"exercise_ind":"ExerciseSynthetizedData","platform_id":"Sample","mission_id":0,"job_id":0,"segments":[{"FreeTextSegment":{"originator_id":"Tester","recipient_id":"User","text":"This is a sample GMTI message containing a single FreeTextSegment. This message is intended for demonstration of the GMTI schema only."}}, {"AnotherSegment": { "originator_id": "Tester" }}]}}"#;

    let obj: serde_json::Value = serde_json::from_str(gmti_json).expect("failed to parse");
    let res = find_in_json(&obj, &mut "packet.segments.#".split("."));
    assert!(res.is_some());
    eprintln!("{}", serde_json::to_string(&res).unwrap());
}

fn get_attributes(content: &[u8], expand: &[String]) -> Result<std::collections::HashMap<String, Vec<u8>>, Box<dyn std::error::Error>> {
    let mut res: HashMap<String, Vec<u8>> = Default::default();

    let string = std::str::from_utf8(content)?;
    let json = serde_json::from_str::<serde_json::Value>(string)?;

    for expansion in expand.iter() {
        if let Some(val) = find_in_json(&json, &mut expansion.split('.').map(|s| s.trim())) {
            res.insert(expansion.to_string(), 
                serde_json::to_vec(&val).expect("impossible: failed to serialize Value"));
        }
    }

    Ok(res)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let port = std::env::var("TETHYS_PROPERTY_PORT").expect("env var TETHYS_PROPERTY_PORT not defined");
    let expansions = std::env::var("TETHYS_PROPERTY_EXPANSIONS").expect("no expansions set in TETHYS_PROPERTY_EXPANSIONS");
    let addr = format!("0.0.0.0:{}", port).parse()?;
    debug!("listening on {}", &addr);
    let expand: Vec<String> = expansions.split(",").map(|s| s.trim().to_string()).collect::<Vec<String>>();
    let jason = JsonArgonauts { expand };

    run_server(jason, addr).await
}
