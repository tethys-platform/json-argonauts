JSON Argonauts
==============

A Tethys processor 

It looks at JSON message payload, parses it and decorates it with message attributes given in `expand` paths.

I.e. message: `{id: "hello"}` will be decorated with Tethys attribute ("id": "hello") if we configure the service with `expand: ["id"]`;

